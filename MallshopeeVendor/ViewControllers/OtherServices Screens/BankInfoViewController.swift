//
//  BankInfoViewController.swift
//  MallshopeeVendor
//
//  Created by goyal on 14/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import Foundation
import UIKit

class BankInfoViewController: UIViewController {
    
    //MARK: OUTLETS
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view5: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setBorderColor(view: view1)
        setBorderColor(view: view2)
        setBorderColor(view: view3)
        setBorderColor(view: view4)
        setBorderColor(view: view5)
        
    }
    
    //MARK: FUNCTIONS
    func setBorderColor(view : UIView) {
        view.layer.borderColor = lightGray.cgColor
    }
    @IBAction func btnMenuTapped(_ sender: Any) {
        
        self.sideMenuViewController?.presentLeftMenuViewController()
        
    }
}
