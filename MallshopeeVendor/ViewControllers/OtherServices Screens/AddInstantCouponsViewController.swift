//
//  AddInstantCouponsViewController.swift
//  MallshopeeVendor
//
//  Created by user on 16/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
import TagListView

class AddInstantCouponsViewController: UIViewController {
    
    //MARK: variable
    var value = ["Coupon Code","Coupon Value","Coupon Type","Products"]
    
    //MARK: OUTLETS
    @IBOutlet var tableView: UITableView!
    @IBOutlet var footerView: UIView!
    @IBOutlet var btnFooter: UIButton!
    
    //MARK: ACTIONS
    @IBAction func actionSave(_ sender: Any) {

       
        
    }
    //btnMenuTapped
    @IBAction func btnMenuTapped(_ sender: UIButton) {
        
      self.navigationController?.popViewController(animated: true)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        saveBtn(btn: btnFooter)
        self.tableView.tableFooterView = footerView
    }
    //ViewLAyout
    func viewLayout(view: UIView!) {
        view.layer.cornerRadius = 5
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 0.5
        view.layer.masksToBounds = false
    }
    //SaveButton
    func saveBtn(btn:UIButton) {
        
        btn.tintColor = UIColor.white
        btn.setTitle("SAVE", for: .normal)
        btn.layer.cornerRadius = 5
        btn.backgroundColor = appColor
    }
    //TagListView
    func tagListView(tagListView: TagListView!) {
        tagListView.addTag("Shirts")
        tagListView.cornerRadius = 10
        
    }
    
}
extension AddInstantCouponsViewController: UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! AddInstantCouponsTableViewCell
            cell.txtfieldcell1.attributedPlaceholder = NSAttributedString(string: value[indexPath.row])
            viewLayout(view: cell.cell1View)
            if(indexPath.row == 2 || indexPath.row == 3) {
                cell.downimgCell1.isHidden = false
            }
            else {
                cell.downimgCell1.isHidden = true
            }
            return cell
        }
            
        else if(indexPath.row == 4) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! AddInstantCouponsTableViewCell
            tagListView(tagListView: cell.tagListView)
            return cell
        }
     
        else if(indexPath.row == 5) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath) as! AddInstantCouponsTableViewCell
            viewLayout(view: cell.cell3View)
            cell.txtView.delegate = self
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell4", for: indexPath) as! AddInstantCouponsTableViewCell
        viewLayout(view: cell.startDateView)
        viewLayout(view: cell.startDateView2)
        return cell
    }
    
    
    //textViewDelegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView.textColor == UIColor.lightGray) {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if(textView.text.isEmpty) {
            textView.text = "Description"
            textView.textColor = UIColor.lightGray
        }
    }
}

