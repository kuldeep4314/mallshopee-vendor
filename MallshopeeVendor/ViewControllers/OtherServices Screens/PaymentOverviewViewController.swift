//
//  PaymentOverviewViewController.swift
//  MallshopeeVendor
//
//  Created by user on 15/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class PaymentOverviewViewController: UIViewController {

    //MARK: OUTLET
    
    let paymentArray = ["Next Payment","Last Payment","Total Outstanding Payment","Unbilled Orders"]
    
    
    let paymentMsgArray = ["Estimated value of next payment. This may change due to returns that come in before the next payout.","These payments have been initiated and may take up to 48 hours to reflect in your bank account.","Total amount you are to receive from Flipkart for dispatched orders. It includes the 'Next Payment' amount shown above.","Thease are orders yet to dispatched. Once dispatched, payments will be scheduled and they will reflect in Total Outstanding Payments' section."]
    
    @IBOutlet var tableView: UITableView!
    
    //btnMenuTapped
    @IBAction func btnMenuTapped(_ sender: UIButton) {
        
        self.sideMenuViewController!.presentLeftMenuViewController()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
 self.tableView.tableFooterView = UIView()
    }
 
}
extension PaymentOverviewViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == 0) {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! PaymentOverviewTableViewCell
        cell.paymentLabel.text = paymentArray[indexPath.row]
        cell.msgLabel.text = paymentMsgArray[indexPath.row]
          cell.packageImg.isHidden = true
        return cell
        }
            
        else if(indexPath.row == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! PaymentOverviewTableViewCell
            cell.lastPayment.text = paymentArray[indexPath.row]
            cell.msgLastPayment.text = paymentMsgArray[indexPath.row]
            
            return cell
        }
            
        else if(indexPath.row == 2){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! PaymentOverviewTableViewCell
            cell.paymentLabel.text = paymentArray[indexPath.row]
            cell.msgLabel.text = paymentMsgArray[indexPath.row]
            cell.date.text = ""
            
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath) as! PaymentOverviewTableViewCell
            cell.unbilledOrders.text = paymentArray[indexPath.row]
            cell.msgUnbilledOrder.text = paymentMsgArray[indexPath.row]
            cell.newOrder.text = "Download the new Orders report"
         return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NextPaymentViewController") as! NextPaymentViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
