//
//  AddBankInfoViewController.swift
//  MallshopeeVendor
//
//  Created by user on 13/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class AddTaxInfoViewController: UIViewController {

    //MARK: OUTLETS
    @IBOutlet var provitionalView: UIView!
    
    @IBOutlet var panView: UIView!
    @IBOutlet var categoryView: UIView!
    @IBOutlet var unitView: UIView!
    @IBOutlet var exciseView: UIView!
    @IBOutlet var igstView: UIView!
    @IBOutlet var cgstView: UIView!
    @IBOutlet var sgstView: UIView!
    @IBOutlet var compensationView: UIView!
    @IBOutlet var savebtn: UIButton!
    
    @IBAction func saveButtonAction(_ sender: Any) {
     
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
    }
    @IBAction func btnMenuTapped(_ sender: Any) {
        self.sideMenuViewController?.presentLeftMenuViewController()
    }
    //setup
    func setup() {
    viewlayout(view: provitionalView)
    viewlayout(view: panView)
    viewlayout(view: categoryView)
    viewlayout(view: unitView)
    viewlayout(view: exciseView)
    viewlayout(view: igstView)
    viewlayout(view: cgstView)
    viewlayout(view: sgstView)
    viewlayout(view: compensationView)
      btnLayout(btn: savebtn)
        
    }
    
    // viewLayout
    func viewlayout(view: UIView!) {
        view.layer.cornerRadius = 5
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 0.5
        view.layer.masksToBounds = false
    }
    
    //btnLayout
    func btnLayout(btn: UIButton!) {
        btn.layer.cornerRadius = 5
        btn.layer.masksToBounds = true
        
    }
}
