//
//  PackagesViewController.swift
//  MallshopeeVendor
//
//  Created by goyal on 16/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import Foundation
import UIKit

class PackagesViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {
    
    //MARK: VARIABLES
    
    var array : [String] = ["Free", "Basic", "Gold", "Platinum"]
    

    var array1 : [String] = ["Just start creating: get a free site and be on your way to publishing your content in less than five minutes.","All Services Offered in gold package", "No. of Leads will increase. Will be alloted by Admin.", "NO SMS and Email will sent for targeted promotion.", "High end predicive analytics.", "No SEO Marketing", "Web Banners will be created for malls to run at our website. No. of Web Banners will be alloted by Admin. There has to be a link in which Mall can share the content/requirement/files for which banners needs to be made."]
    
    var index = 0
    
    //MARK: OUTLETS
    
    @IBOutlet weak var viewCollection: UICollectionView!
    
    @IBOutlet weak var viewTable: UITableView!
    @IBOutlet weak var viewTable2: UITableView!
    @IBOutlet weak var viewTable3: UITableView!
    @IBOutlet weak var viewTable4: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
       
        self.viewTable.tableFooterView = UIView()
        
    }
    @IBAction func btnMenuTapped(_ sender: Any) {
        self.sideMenuViewController?.presentLeftMenuViewController()
    }
    
    
    //MARK: COLLECTION VIEW FUNCTIONS
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = viewCollection.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! PackagesCollectionViewCell
        cell.lblCell.text = array[indexPath.row]
        
        if(index == indexPath.row) {
            cell.lblCell.textColor = appColor
            cell.lblLine.isHidden = false
            cell.lblLine.backgroundColor = appColor
        }
        else {
            cell.lblCell.textColor = UIColor.black
            cell.lblLine.isHidden = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
       
        let multiplier = CGFloat(indexPath.row)
        let cg = self.scrollView.frame.size.width * multiplier
        scrollView.scrollRectToVisible(CGRect.init(x: cg, y: 0, width: self.scrollView.frame.size.width, height: self.scrollView.frame.size.height), animated: true)
        
        
        self.viewCollection.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        index = indexPath.row
        
        self.viewCollection.reloadData()
        
    }
    
    
    //MARK: TABLE VIEW FUNCTIONS
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: viewTable.frame.size.width , height: 60))
        
        customView.backgroundColor = UIColor.white
        
        let button = UIButton(frame: CGRect(x: 15, y: 20, width: viewTable.frame.size.width - 30, height: 60))
        button.setTitle("START WITH FREE", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 25)
        button.backgroundColor = appColor
        button.layer.cornerRadius = 8
        //button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        customView.addSubview(button)
        
        return customView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.row > 0 {
            let cell = viewTable.dequeueReusableCell(withIdentifier: "Cell2", for: indexPath) as! FreepackageTableViewCell
            cell.label2.setValue("light", forKey: "nuiClass")
            cell.label2.text = array1[indexPath.row]
            
            return cell
        }
        else {
            let cell = viewTable.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath) as! FreepackageTableViewCell
            cell.label1.setValue("regular", forKey: "nuiClass")
            cell.label1.text = array1[indexPath.row]
            
            return cell
        }
    }
    
    //MARK: SCROLL VIEW FUNCTION

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == self.scrollView {
            let indexOfPage = scrollView.contentOffset.x/scrollView.frame.size.width
            
            self.viewCollection.scrollToItem(at: IndexPath.init(row: Int(indexOfPage), section: 0  ), at: .centeredHorizontally, animated: true)
            index = Int(indexOfPage)
            self.viewCollection.reloadData()
            
            
        }
    }
    
    
}
