//
//  StoreInfoViewController.swift
//  MallshopeeVendor
//
//  Created by goyal on 14/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import Foundation
import UIKit

class StoreInfoViewController: UIViewController {
    
    @IBOutlet var views: [UIView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        views.forEach() {
            $0.layer.borderColor = lightGray.cgColor
        }
    }
    
    
    
    @IBAction func btnMenuTapped(_ sender: Any) {
        
        self.sideMenuViewController?.presentLeftMenuViewController()
        
    }
    
    
}
