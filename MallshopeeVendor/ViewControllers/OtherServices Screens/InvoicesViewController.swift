//
//  InvoicesViewController.swift
//  M/Users/goyal/Downloads/MallshopeeVendor/MallshopeeVendor/ReviewScreens/InvoicesViewController.swiftallshopeeVendor
//
//  Created by goyal on 15/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import Foundation
import UIKit

class InvoicesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: OUTLETS
    
    @IBOutlet weak var viewTable: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewTable.rowHeight = UITableViewAutomaticDimension
        self.viewTable.tableFooterView = UIView()
    }
    @IBAction func btnMenuTapped(_ sender: Any) {
        
        self.sideMenuViewController?.presentLeftMenuViewController()
    }
    
    //MARK: TABLE VIEW FUNCTIONS
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = viewTable.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! InvoicesTableViewCell
        
        cell.viewCell.layer.borderColor = lightGray.cgColor
        cell.viewCell.layer.cornerRadius = 8
        
        return cell
        
    }
    
}
