//
//  PackageMaterialViewController.swift
//  MallshopeeVendor
//
//  Created by user on 14/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class PackageMaterialViewController: UIViewController {

    var value = ["",""]
    //MARK: OUTLETS
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView =  UIView()
    }
    //btnMenuTapped
    @IBAction func btnMenuTapped(_ sender: UIButton) {
        
        self.sideMenuViewController!.presentLeftMenuViewController()
        
    }
//viewLayout
   func viewLayout(view: UIView) {
    view.layer.cornerRadius = 5
    
    }
}

extension PackageMaterialViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return value.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PackageMaterialTableViewCell
        viewLayout(view: cell.imgView)
        viewLayout(view: cell.purchaseBtn)
        return cell
    }
    
}
