//
//  PackagesCollectionViewCell.swift
//  MallshopeeVendor
//
//  Created by goyal on 16/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class PackagesCollectionViewCell: UICollectionViewCell {
    
    //MARK: OUTLETS
    
    @IBOutlet weak var lblCell: UILabel!
    
    @IBOutlet weak var lblLine: UILabel!
    
    
}
