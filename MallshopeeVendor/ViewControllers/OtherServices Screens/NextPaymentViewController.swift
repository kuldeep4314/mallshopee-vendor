//
//  NextPaymentViewController.swift
//  MallshopeeVendor
//
//  Created by goyal on 21/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import Foundation
import UIKit

class NextPaymentViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: VARIABLES
    
    var array1 = ["", "Order Transaction:", "", "Storage & Recall Transaction:", "", "Non Order SPF Transaction:", "", "Ads Transaction:", ""]
    
    
    var arrayId_Lbl1 = ["", "", "Product Id:", "", "Listing Id:"]
    
    
    var arrayId_Lbl2 = ["", "", "Order Id:", "", "Recall Id:"]
    
    var array_Lbl3 = ["", "", "Dispatch Date:", "", "Status Code:", "", ""]
    var array_Lbl3Value = ["", "", "Jan12, 2018", "", "45789"]
    
    var array_Lbl4 = ["", "", "Description", "", "Services:"]
    var array_Lbl4Value = ["", "", "", "", "45789"]
    
    //MARK: OUTLETS
    
    @IBOutlet weak var viewTable: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewTable.tableFooterView = UIView()
        searchTextField.leftview()
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: TABLE VIEW FUNCTIONS
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = viewTable.cellForRow(at: indexPath)
        cell?.backgroundColor = UIColor.white
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 9
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = viewTable.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath) as! NextPaymentTableViewCell
            cell.viewCell1.layer.borderColor = lightGray.cgColor
            return cell
        }
            
        else if (indexPath.row == 1 || indexPath.row == 3 || indexPath.row == 5 || indexPath.row == 7){
            let cell = viewTable.dequeueReusableCell(withIdentifier: "Cell2", for: indexPath) as! NextPaymentTableViewCell
            
            cell.viewCell2.layer.borderColor = lightGray.cgColor
            
            cell.lbl1Cell2.text = array1[indexPath.row]
            
            return cell
        }
        else if indexPath.row == 6 {
            let cell = viewTable.dequeueReusableCell(withIdentifier: "Cell4", for: indexPath) as! NextPaymentTableViewCell
            return cell
        }
            
        else if indexPath.row == 8 {
            let cell = viewTable.dequeueReusableCell(withIdentifier: "Cell5", for: indexPath) as! NextPaymentTableViewCell
            return cell
        }
            
        else {
            let cell = viewTable.dequeueReusableCell(withIdentifier: "Cell3", for: indexPath) as! NextPaymentTableViewCell
            cell.idLbl1Cell3.text = arrayId_Lbl1[indexPath.row]
            
            cell.idLbl2Cell3.text = arrayId_Lbl2[indexPath.row]
            
            cell.lbl3.text = array_Lbl3[indexPath.row]
            
            cell.lbl3Value.text = array_Lbl3Value[indexPath.row]
            
            cell.lbl4.text = array_Lbl4[indexPath.row]
            
            cell.lbl4Value.text = array_Lbl4Value[indexPath.row]
            
            
            return cell
        }
        
    }
    
    
}
