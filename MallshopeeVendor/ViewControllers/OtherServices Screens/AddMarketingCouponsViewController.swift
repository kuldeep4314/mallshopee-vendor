//
//  AddMarketingCouponsViewController.swift
//  MallshopeeVendor
//
//  Created by user on 16/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
import TagListView

class AddMarketingCouponsViewController: UIViewController {

    //MARK: variable
    var tagListarray = ["Shirt"]
    var value = ["Coupon Code","Coupon Value(%)","Coupon Value","Coupon QTY","Coupon Image","Coupon Type","Products"]
    var datetxt = UITextField()
    var timetxt = UITextField()
    
    //MARK: OUTLETS
    @IBOutlet var tableView: UITableView!
    @IBOutlet var footerView: UIView!
    @IBOutlet var btnFooter: UIButton!
    
    //MARK: ACTIONS
    @IBAction func saveAction(_ sender: UIButton) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        saveBtn(btn: btnFooter)
        self.tableView.tableFooterView = footerView
    }
    
    //btnMenuTapped
    @IBAction func btnMenuTapped(_ sender: UIButton) {
        self.sideMenuViewController!.presentLeftMenuViewController()
    }
    
    //ViewLAyout
    func viewLayout(view: UIView!) {
        view.layer.cornerRadius = 5
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 0.5
        view.layer.masksToBounds = false
    }
    //SaveButton
    func saveBtn(btn:UIButton) {
        btn.tintColor = UIColor.white
        btn.setTitle("SAVE", for: .normal)
        btn.layer.cornerRadius = 5
        btn.backgroundColor = appColor
    }

}
extension AddMarketingCouponsViewController: UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {
    
 
    //tableViewFunctions
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      if (indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! AddMarketingCouponsTableViewCell
            cell.txtfieldcell1.attributedPlaceholder = NSAttributedString(string: value[indexPath.row])
            viewLayout(view: cell.cell1View)
            if(indexPath.row == 2) {
                cell.downimgCell1.isHidden = false
            }
            else {
                cell.downimgCell1.isHidden = true
            }
            return cell
        }
        
      else if(indexPath.row == 4) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! AddMarketingCouponsTableViewCell
            cell.labelCell2.text = value[indexPath.row]
            return cell
     }
        
      else if(indexPath.row == 5 || indexPath.row == 6) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! AddMarketingCouponsTableViewCell
            cell.txtfieldcell1.attributedPlaceholder = NSAttributedString(string: value[indexPath.row])
            viewLayout(view: cell.cell1View)
            return cell
     }
      else if(indexPath.row == 7) {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell5", for: indexPath) as! AddMarketingCouponsTableViewCell
        cell.tagarray = tagListarray
       
        return cell
      }
      else if(indexPath.row == 8) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath) as! AddMarketingCouponsTableViewCell
        cell.txtView.text = datetxt.text
            viewLayout(view: cell.cell3View)
        cell.txtView.delegate = self
        
            return cell
     }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell4", for: indexPath) as! AddMarketingCouponsTableViewCell
        viewLayout(view: cell.startDateView)
        viewLayout(view: cell.startDateView2)
       
       
       
        return cell
    }
    

    //textViewDelegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView.textColor == UIColor.lightGray) {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if(textView.text.isEmpty) {
            textView.text = "Description"
            textView.textColor = UIColor.lightGray
        }
    }
}
