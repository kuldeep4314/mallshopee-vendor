//
//  InstantCouponViewController.swift
//  MallshopeeVendor
//
//  Created by goyal on 15/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import Foundation
import UIKit

class InstantCouponViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    //MARK: OUTLETS
    
    @IBOutlet weak var viewTable: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var statusTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewTable.rowHeight = UITableViewAutomaticDimension
        searchTextField.leftview()
        statusTextField.leftview()
        self.viewTable.tableFooterView = UIView()
    }
    
    @IBAction func btnAddTapped(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddInstantCouponsViewController") as! AddInstantCouponsViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    //MARK: TABLE VIEW FUNCTIONS
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = viewTable.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! InstantCouponTableViewCell
        return cell
    }
    @IBAction func btnMenuTapped(_ sender: Any) {
        self.sideMenuViewController?.presentLeftMenuViewController()
    }
    
    
  
}
