//
//  AddInstantCouponsTableViewCell.swift
//  MallshopeeVendor
//
//  Created by user on 16/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
import TagListView

class AddInstantCouponsTableViewCell: UITableViewCell {

    //MARK: OUTLETS
    
    @IBOutlet var imgCell1: UIImageView!
    @IBOutlet var txtfieldcell1: UITextField!
    @IBOutlet var downimgCell1: UIImageView!
    
    @IBOutlet var tagListView: TagListView!
    @IBOutlet var cell1View: UIView!
    @IBOutlet var cell3View: UIView!
    @IBOutlet var startDateView: UIView!
    @IBOutlet var startDateView2: UIView!
    @IBOutlet var txtView: UITextView!
}
