//
//  FreepackageTableViewCell.swift
//  MallshopeeVendor
//
//  Created by goyal on 16/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class FreepackageTableViewCell: UITableViewCell {

    //MARK: OUTLETS
    
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
