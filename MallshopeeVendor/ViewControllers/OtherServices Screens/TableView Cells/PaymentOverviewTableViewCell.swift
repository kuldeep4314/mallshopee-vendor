//
//  PaymentOverviewTableViewCell.swift
//  MallshopeeVendor
//
//  Created by user on 15/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class PaymentOverviewTableViewCell: UITableViewCell {

    //MARK: OUTLETS
    @IBOutlet var paymentLabel: UILabel!
    @IBOutlet var msgLabel: UILabel!
    @IBOutlet var packageImg: UIImageView!
    @IBOutlet var postpaidImg: UIImageView!
    @IBOutlet var prepaidImg: UIImageView!
    @IBOutlet var date: UILabel!
    
    @IBOutlet var lastPayment: UILabel!
    @IBOutlet var msgLastPayment: UILabel!
    
    @IBOutlet var unbilledOrders: UILabel!
    @IBOutlet var msgUnbilledOrder: UILabel!
    @IBOutlet var newOrder: UILabel!
}
