//
//  PackageMaterialTableViewCell.swift
//  MallshopeeVendor
//
//  Created by user on 14/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class PackageMaterialTableViewCell: UITableViewCell {

    //MARK: OUTLETS
    @IBOutlet var imgView: UIImageView!
    @IBOutlet var purchaseBtn: UIButton!
    
}
