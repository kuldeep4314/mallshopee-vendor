//
//  AddMarketingCouponsTableViewCell.swift
//  MallshopeeVendor
//
//  Created by user on 16/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
import TagListView
class AddMarketingCouponsTableViewCell: UITableViewCell {

    var tagarray = [String](){
        didSet {
            for tag in tagarray{
                self.tagListView.addTag(tag)
                
            }
        }
    }
    //MARK: OUTLETS
    
    @IBOutlet var imgCell1: UIImageView!
    @IBOutlet var txtfieldcell1: UITextField!
    @IBOutlet var downimgCell1: UIImageView!
    @IBOutlet var labelCell2: UILabel!
    
    @IBOutlet var tagListView: TagListView!
    
    @IBOutlet var startdateTxtField: UITextField!
    @IBOutlet var startdate2TxtField: UITextField!
    
    @IBOutlet var cell1View: UIView!
    @IBOutlet var cell3View: UIView!
    @IBOutlet var startDateView: UIView!
    @IBOutlet var startDateView2: UIView!
    @IBOutlet var txtView: UITextView!
}
