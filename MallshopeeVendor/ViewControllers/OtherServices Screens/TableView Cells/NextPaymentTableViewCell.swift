//
//  NextPaymentTableViewCell.swift
//  MallshopeeVendor
//
//  Created by goyal on 21/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class NextPaymentTableViewCell: UITableViewCell {

    //MARK: OUTLETS
    
    @IBOutlet weak var viewCell1: UIView!
    @IBOutlet weak var viewCell2: UIView!
    
    @IBOutlet weak var lbl1Cell2: UILabel!
    @IBOutlet weak var lblValueCell2: UILabel!
    
    @IBOutlet weak var idLbl1Cell3: UILabel!
    @IBOutlet weak var id1Value: UILabel!
    @IBOutlet weak var idLbl2Cell3: UILabel!
    @IBOutlet weak var id2Value: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lbl3Value: UILabel!
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var lbl4Value: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
