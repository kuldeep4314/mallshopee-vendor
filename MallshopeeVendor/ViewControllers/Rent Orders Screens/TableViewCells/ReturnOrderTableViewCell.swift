//
//  ReturnOrderTableViewCell.swift
//  MallshopeeVendor
//
//  Created by goyal on 20/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class ReturnOrderTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var lblProductId: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblColor: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnColor: UIButton!
    @IBOutlet weak var lblShipingValue: UILabel!
    @IBOutlet weak var lblSKUValue: UILabel!
    @IBOutlet weak var lblSizeValue: UILabel!
    @IBOutlet weak var lblQuantityValue: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var subOrderValue: UILabel!
    
    
    @IBOutlet weak var btnHandOver: UIButton!
    
    @IBOutlet weak var btnReadyToTransit: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

