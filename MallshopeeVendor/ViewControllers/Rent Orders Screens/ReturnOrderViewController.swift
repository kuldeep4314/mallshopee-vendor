//
//  ReturnOrderViewController.swift
//  MallshopeeVendor
//
//  Created by goyal on 20/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class ReturnOrderViewController: UIViewController ,UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    let arrNames = ["Return Orders","Package Picked", "Shipped","In Transit","Received"]
    
    @IBOutlet weak var txtSearch: UITextField!
    var indexs = 0
    @IBOutlet weak var tblReturnOrders: UITableView!
    
    @IBOutlet weak var tblReceived: UITableView!
    @IBOutlet weak var tblShipped: UITableView!
    @IBOutlet weak var tblPackagePicked: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var tblInTransit: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtSearch.leftview()
        
        tblReturnOrders.rowHeight = UITableViewAutomaticDimension
        tblReturnOrders.tableFooterView = UIView()
        tblPackagePicked.rowHeight = UITableViewAutomaticDimension
        tblPackagePicked.tableFooterView = UIView()
        
        tblShipped.rowHeight = UITableViewAutomaticDimension
        tblShipped.tableFooterView = UIView()
        
        tblInTransit.rowHeight = UITableViewAutomaticDimension
        tblInTransit.tableFooterView = UIView()
        
        tblReceived.rowHeight = UITableViewAutomaticDimension
        tblReceived.tableFooterView = UIView()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnMenuTapped(_ sender: Any) {
        self.sideMenuViewController!.presentLeftMenuViewController()
    }
    @IBAction func btnFilterTapped(_ sender: Any) {
        let story = UIStoryboard.init(name: "Menu", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //MARK:- SrollView Delegates
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView == self.scrollView{
            let indexOfPage = scrollView.contentOffset.x / scrollView.frame.size.width
            
            self.collectionView.scrollToItem(at: IndexPath.init(row: Int(indexOfPage), section: 0  ), at: .centeredHorizontally, animated: true)
            indexs = Int(indexOfPage)
            self.collectionView.reloadData()
            
            
        }
    }
    
    //MARK:- CollectionView Datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrNames.count
    }
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cvCell", for: indexPath)
        
        let lbl = cell.viewWithTag(1) as! UILabel
        lbl.text = self.arrNames[indexPath.row]
        if indexs == indexPath.row {
            
            lbl.textColor = appColor
        }
        else {
            lbl.textColor = UIColor.black
            
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        
        let multiplier = CGFloat(indexPath.row)
        let cg = self.scrollView.frame.size.width * multiplier
        scrollView.scrollRectToVisible(CGRect.init(x: cg, y: 0, width: self.scrollView.frame.size.width, height: self.scrollView.frame.size.height), animated: true)
        
        
        self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        indexs = indexPath.row
        self.collectionView.reloadData()
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == 0 {
             return CGSize(width: 120, height: 44)
        }
        else{
             return CGSize(width: 110, height: 44)
        }
        
       
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    
}


extension ReturnOrderViewController : UITableViewDataSource , UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return  1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblReturnOrders {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            
            
            return cell
        }
        else  if tableView == tblPackagePicked {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            
            
            return cell
        }
        else  if tableView == tblShipped {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            
            
            return cell
        }
            
        else  if tableView == tblInTransit {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            
            
            return cell
        }
        else  if tableView == tblReceived {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            
            
            return cell
        }
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    
}
