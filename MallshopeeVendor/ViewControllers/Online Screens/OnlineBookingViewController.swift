//
//  OnlineBookingViewController.swift
//  MallshopeeVendor
//
//  Created by user on 20/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class OnlineBookingViewController: UIViewController {

    //MARK: VARIABLES
    var value = ["New Orders","In Kitchen","In Transit","Handed over","Delivered","Completed"]
    var item = ["Icecream(2)","Icecream(2)"]
    var factoryName = ["Factory Name","Factory Name"]
    var categoryName = ["Category Name","Category Name"]
    var orderId = ["Order Id: 1298765gf","Order Id: 1298765gf"]
    var index = 0
    
    //MARK: OUTLETS
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var tableView1: UITableView!
    @IBOutlet var tableView2: UITableView!
    @IBOutlet var tableView3: UITableView!
    @IBOutlet var tableView4: UITableView!
    @IBOutlet var tableView5: UITableView!
    @IBOutlet var tableView6: UITableView!
    @IBOutlet var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    //btnMenuTapped
    @IBAction func btnMenuTapped(_ sender: UIButton) {
        
        self.sideMenuViewController!.presentLeftMenuViewController()
        
    }
    //MARK: imageLayout
    func imgLayout(img: UIImageView) {
        img.layer.cornerRadius = 5
    }
    
    //ViewLAyout
    func viewLayout(view: UIView!) {
        view.layer.cornerRadius = 5
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 0.5
        view.layer.masksToBounds = false
    }

}

extension OnlineBookingViewController: UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {
    
    //CollectionviewFunctions
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "menuCell", for: indexPath) as? OnlineBookingCollectionViewCell
        cell?.lbl.text = value[indexPath.row]
        if(index == indexPath.row) {
            cell?.lbl.textColor = appColor
            
        }
        else {
            cell?.lbl.textColor = UIColor.black
        }

        return cell!
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: 80
            , height: 30);
    }
  
    //MARK: COLLECTIONVIEW DIDSELECTITEM
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let multiplier = CGFloat(indexPath.row)
        let cg = self.scrollView.frame.size.width * multiplier
        scrollView.setContentOffset(CGPoint.init(x: cg, y: 0), animated: true)
        scrollView.scrollRectToVisible(CGRect.init(x: cg, y: 0, width: self.scrollView.frame.size.width, height: self.scrollView.frame.size.height), animated: true)
        self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        index = indexPath.row
        self.collectionView.reloadData()
        

    }
    
    //MARK: TableViewFunctions
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return item.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == tableView1){
        let cell = tableView.dequeueReusableCell(withIdentifier: "Itemcell", for: indexPath) as? OnlineBookingTableViewCell
        cell?.lblItem.text = item[indexPath.row]
        cell?.lblItemvalue.text = "$50"
        return cell!
        }
        else if(tableView == tableView2) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Itemcell", for: indexPath) as? OnlineBookingTableViewCell
            cell?.lblItem.text = item[indexPath.row]
            cell?.lblItemvalue.text = "$50"
            return cell!
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Itemcell", for: indexPath) as? OnlineBookingTableViewCell
            cell?.lblItem.text = item[indexPath.row]
            cell?.lblItemvalue.text = "$50"
            return cell!
        }
        
      }
    //MARK:- viewforHeader Function
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "header") as? OnlineBookingTableViewCell!
        cell?.lblFactory.text = factoryName[section]
        cell?.lblCategory.text = categoryName[section]
        cell?.lblOrderId.text = orderId[section]
        cell?.lblTotalItem.text = "4"
        imgLayout(img: (cell?.img!)!)
        return cell?.contentView
    }
    //MARK: viewforfooter
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        
        if(tableView == tableView2) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "footer2") as? OnlineBookingTableViewCell
            cell?.btnfooterview2.setTitle("READY TO PACK", for: .normal)
            viewLayout(view: cell?.footerView2)
            return cell?.contentView

        }
        else if(tableView == tableView4) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "footer2") as? OnlineBookingTableViewCell
            cell?.btnfooterview2.setTitle("DELIVERED", for: .normal)
            viewLayout(view: cell?.footerView2)
           
            return cell?.contentView
            
        }
        else if(tableView == tableView5) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "footer2") as? OnlineBookingTableViewCell
            cell?.btnfooterview2.setTitle("COMPLETED", for: .normal)
            viewLayout(view: cell?.footerView2)
            return cell?.contentView
            
        }
        else if(tableView == tableView1) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "footer") as? OnlineBookingTableViewCell
        viewLayout(view: cell?.viewfooter)
        return cell?.contentView
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "footer2") as? OnlineBookingTableViewCell
            viewLayout(view: cell?.footerView2)
            return cell?.contentView
        }
    }
        
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 70
    }
    //MARK: scrollViewDelegates
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == self.scrollView {
            let indexOfPage = scrollView.contentOffset.x/scrollView.frame.size.width
            
            self.collectionView.scrollToItem(at: IndexPath.init(row: Int(indexOfPage), section: 0  ), at: .centeredHorizontally, animated: true)
            index = Int(indexOfPage)
            self.collectionView.reloadData()
            
            
        }
    }
}
