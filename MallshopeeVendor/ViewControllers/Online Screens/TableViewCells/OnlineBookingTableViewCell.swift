//
//  OnlineBookingTableViewCell.swift
//  MallshopeeVendor
//
//  Created by user on 20/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class OnlineBookingTableViewCell: UITableViewCell {

    //MARK: OUTLETS
    @IBOutlet var lblItem: UILabel!
    @IBOutlet var img: UIImageView!
    @IBOutlet var lblFactory: UILabel!
    @IBOutlet var lblCategory: UILabel!
    @IBOutlet var lblOrderId: UILabel!
    @IBOutlet var lblTotalItem: UILabel!
    @IBOutlet var viewfooter: UIView!
    @IBOutlet var lblItemvalue: UILabel!
    @IBOutlet var btnaction: UIButton!
    @IBOutlet var btndecline: UIButton!
    @IBOutlet var footerView2: UIView!
    @IBOutlet var btnfooterview2: UIButton!
    
}
