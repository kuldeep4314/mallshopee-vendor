//
//  ProductOrderDetailViewController.swift
//  MallshopeeVendor
//
//  Created by user on 21/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class ProductOrderDetailViewController: UIViewController {

    //MARK: OUTLETS
    
    @IBOutlet var lblproductName: UILabel!
    @IBOutlet var lblproductId: UILabel!
    @IBOutlet var lblOrder: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblShippingName: UILabel!
    @IBOutlet var lblSKUid: UILabel!
    @IBOutlet var lblprice: UILabel!
    @IBOutlet var lblsizeNo: UILabel!
    @IBOutlet var lblQuantityNo: UILabel!
    @IBOutlet var lblFsnNo: UILabel!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblPhone: UILabel!
    @IBOutlet var lblPin: UILabel!
    @IBOutlet var viewBottom: UIView!
    @IBOutlet var btnAccept: UIButton!
    @IBOutlet var btnReject: UIButton!
    @IBOutlet var imgColor: UIImageView!
    
    //ViewLAyout
    func viewLayout(view: UIView!) {
        view.layer.cornerRadius = 5
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 0.5
        view.layer.masksToBounds = false
    }
  
    //btnMenuTapped
    @IBAction func btnMenuTapped(_ sender: UIButton) {
        
       self.navigationController?.popViewController(animated: true)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
    }

    //MARK: SETUP
    func setup() {
        self.lblproductName.text = NSLocalizedString("ProductName", comment: "ProductName")
        self.lblproductId.text = NSLocalizedString("productId", comment: "productId")
        self.lblOrder.text = NSLocalizedString("Order", comment: "Order")
        self.lblDate.text = NSLocalizedString("Date", comment: "Date")
         self.lblShippingName.text = NSLocalizedString("ShippingName", comment: "ShippingName")
         self.lblSKUid.text = NSLocalizedString("SKUid", comment: "SKUid")
         self.lblprice.text = NSLocalizedString("price", comment: "price")
        self.lblsizeNo.text = NSLocalizedString("sizeNo", comment: "sizeNo")
        self.lblQuantityNo.text = NSLocalizedString("QuantityNo", comment: "QuantityNo")
        self.lblFsnNo.text = NSLocalizedString("FsnNo", comment: "FsnNo")
        self.lblName.text = NSLocalizedString("Name", comment: "Name")
        self.lblPhone.text = NSLocalizedString("Phone", comment: "Phone")
        self.lblPin.text = NSLocalizedString("Pin", comment: "Pin")
        imgColor.layer.cornerRadius = self.imgColor.frame.width / 2
        viewLayout(view: viewBottom)
    }

}
