//
//  ProductIndentifiersViewController.swift
//  MallshopeeVendor
//
//  Created by goyal on 14/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class ProductIndentifiersViewController:  UIViewController,UITableViewDelegate,UITableViewDataSource ,UITextViewDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    let array = ["Product Name","SKU Generate","","SKU Generate","Model No.","Brand Name","Category","Last Level Category","UPC","EAN","ISBN","Enter Your Description","Save"]
     let textViewPlaceHolder = "Enter Your Description"
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellWithText", for: indexPath) as! ProductIndentifierTableViewCell
            cell.txtValue.placeholder = array[indexPath.row]
            return cell
            
        }
            
        else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellSKU", for: indexPath) as! ProductIndentifierTableViewCell
            cell.lblTitle.text = array[indexPath.row]
            return cell
            
        }
            
        else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellManualAutomatic", for: indexPath) as! ProductIndentifierTableViewCell
            
            return cell
            
        }
            
        else if indexPath.row > 2 && indexPath.row < 11{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellWithText", for: indexPath) as! ProductIndentifierTableViewCell
            cell.txtValue.placeholder = array[indexPath.row]
            return cell
            
        }
        else if indexPath.row == 11 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellTxtView", for: indexPath) as! ProductIndentifierTableViewCell
            cell.txtView.text = textViewPlaceHolder
            cell.txtView.textColor = textGrey
            cell.txtView.delegate = self
            return cell
        }
            
       
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellSave", for: indexPath) as! ProductIndentifierTableViewCell
        
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row < 11 {
            return 45
        }
        else {
            return 100
        }
        
}
    //MARK:- UITEXTVIEW DELEGATES
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.textColor == textGrey
        {
           textView.text = ""
           textView.textColor = UIColor.black
            print("begin working")
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        
        if textView.text.isEmpty {
           textView.text =  textViewPlaceHolder
           textView.textColor = textGrey
            print("end working")
        }
    }

    
}
