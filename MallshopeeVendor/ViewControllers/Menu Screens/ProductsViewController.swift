//
//  ProductsViewController.swift
//  MallshopeeVendor
//
//  Created by goyal on 12/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
import AKSideMenu
class ProductsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var txtStatus: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        txtSearch.leftview()
        txtStatus.leftview()
        self.tableView.tableFooterView = UIView()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.lblTitle.text = "PRODUCTS"
        
        // Do any additional setup after loading the view.
    }
    @IBAction func btnFilterTapped(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as!
        FilterViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnQCTapped(_ sender: Any) {
        let vc = QCView.init(frame: CGRect.init(x: self.view.frame.width - (self.view.frame.width / 3) - 10, y: 56, width:  (self.view.frame.width / 3), height: 170))
        vc.delegaes = self
        self.view.addSubview(vc)
    }
    
    
    
    @objc func btnAddSellingInfoTapped(sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddSellingInfoViewController") as!
        AddSellingInfoViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnMenuTapped(_ sender: UIButton) {
        
        self.sideMenuViewController!.presentLeftMenuViewController()
        
    }
    @IBAction func btnAddProductTapped(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddProductViewController") as! AddProductViewController
  self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    //MARK:- TableView Data Source Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return  1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:  self.lblTitle.text!, for: indexPath) as! ProductsTableViewCell
        
        
        if self.lblTitle.text == "PRODUCTS"{
             cell.btnAddSellingInfo.tag = indexPath.row
            cell.btnAddSellingInfo.addTarget(self, action: #selector(self.btnAddSellingInfoTapped(sender:)), for: .touchUpInside)
        
        }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailViewController") as! ProductDetailViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

extension ProductsViewController : QCFilterDelegates {
    
    func filterQCFunction(title: String){
        self.lblTitle.text = title
        self.tableView.reloadData()
    }
    
}

