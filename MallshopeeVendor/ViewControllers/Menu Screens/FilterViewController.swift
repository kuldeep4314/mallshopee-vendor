//
//  FilterViewController.swift
//  Mallshopee
//
//  Created by goyal on 30/01/18.
//  Copyright © 2018 Avatar Singh. All rights reserved.
//

import UIKit

class FilterViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

     var selectedIndex = 0
    var array = ["Color" :["Red","Blue"], "Size" : ["Small", "Large", "Medium"]]
   
     var selectedRows = [Int]()
    @IBOutlet weak var subTableView: UITableView!
    @IBOutlet weak var mainTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

       self.mainTableView.tableFooterView = UIView()
        self.subTableView.tableFooterView = UIView()
        for _ in 0..<JSON(array)[JSON(array).map({$0.0})[selectedIndex]].count {
            selectedRows.append(-1)
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
   
    
    @IBAction func BackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == mainTableView {
            
            return JSON(array).map({$0.0}).count
        }
        else{
            
            return JSON(array)[JSON(array).map({$0.0})[selectedIndex]].count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if tableView == mainTableView {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "mainCell", for: indexPath)
            let lblTitle =  cell.viewWithTag(1) as! UILabel
            lblTitle.text = JSON(array).map({$0.0})[indexPath.row]
            if indexPath.row == selectedIndex {
                cell.contentView.backgroundColor = appColor
            }
            else{
                cell.contentView.backgroundColor = UIColor.white
            }
            return cell
        }
        else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "subCell", for: indexPath)
            let btnTick = cell.viewWithTag(2) as! UIButton
            
            if selectedRows[indexPath.row] == indexPath.row {
                btnTick.backgroundColor = appColor
            }
            else{
                   btnTick.backgroundColor = UIColor.white
            }
            let lblSubTitle = cell.viewWithTag(3
            ) as! UILabel
            
            lblSubTitle.text = JSON(array)[JSON(array).map({$0.0})[selectedIndex]][indexPath.row].stringValue
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if tableView == self.mainTableView {
              selectedRows.removeAll()
            self.selectedIndex = indexPath.row
            for _ in 0..<JSON(array)[JSON(array).map({$0.0})[selectedIndex]].count {
                selectedRows.append(-1)
            }
            self.mainTableView.reloadData()
            self.subTableView.reloadData()
        }
        else{
          
            if indexPath.row == selectedRows[indexPath.row]{
                 selectedRows[indexPath.row] = -1
            }
            else{
                 selectedRows[indexPath.row] = indexPath.row
            }
            self.subTableView.reloadData()
        }
    }
    
}
