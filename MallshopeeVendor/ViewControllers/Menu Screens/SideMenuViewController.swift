//
//  SideMenuViewController.swift
//  MallshopeeVendor
//
//  Created by goyal on 12/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController, UITableViewDelegate , UITableViewDataSource {

    let array = ["Home" : [] , "My Listings" : ["Product List", "Paid Vouchers", "Create Brochure" ,"Create Deals","Size Chart","Free Offers","Add Products via Upload"],"Product Orders": ["Product Orders","Rent Orders","Trial Orders","Return Orders","Cancel Orders","Online Booking"],"Coupon Management":["Virtual Bargains","Instant Coupons","Marketing Coupons"],"Payments":["Overview","Pervious Payments","Transactions","Invoices","Statements"],"Performace" :["Overview","Growth Opportunities","Reports"],"Page Management":["Events","Store Details","Review","Gallery"],"Account Management":["User Management","Package Material","Edit Profile","My Packages"],"Promotions":["General Tickets","Create Tickets"],"Other Services":["Bank Information","Store  Information","Change Category","Tax Information","Packages"]]
    

    let headerArr = ["Home", "My Listings", "Product Orders", "Coupon Management", "Payments", "Performace", "Page Management", "Account Management", "Promotions", "Other Services"]
    
    @IBOutlet weak var tableView: UITableView!
    var selectedIndex = -1
    override func viewDidLoad() {
        super.viewDidLoad()
     self.tableView.tableFooterView = UIView()
        
      
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
        if selectedIndex  == sender.view?.tag {
            selectedIndex  = -1
        }
        else{
            selectedIndex  = (sender.view?.tag)!
        }
        self.tableView.reloadData()
        if selectedIndex == 0 {
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "Nav")
            self.sideMenuViewController!.setContentViewController(viewController!, animated: true)
            self.sideMenuViewController!.hideMenuViewController()
            
        }
        
        
    }
    //MARK:- TableView Data Source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return headerArr.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     
        if self.selectedIndex == section {
            
            return JSON(array)[headerArr[selectedIndex]].count
        }
        else{
            return  0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let lbl = cell.contentView.viewWithTag(105) as? UILabel
        lbl?.text  = JSON(array)[headerArr[selectedIndex]][indexPath.row].stringValue
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellHeader")
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        
       cell?.contentView.addGestureRecognizer(tap)
       cell?.contentView.tag = section
       cell?.contentView.isUserInteractionEnabled = true
       cell?.contentView.addGestureRecognizer(tap)
        
        
        let lbl = cell?.contentView.viewWithTag(102) as! UILabel
       
              lbl.text =  headerArr[section]
        
        return cell?.contentView
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        
        
        let title = JSON(array)[headerArr[selectedIndex]][indexPath.row].stringValue
        
        if title == "Product List" {
            self.pushViewControllerWithVCdName(story: "Menu", viewController: "ProductsViewController")
        }
        
       else if title == "Paid Vouchers" {
           
             self.pushViewControllerWithStoryBoardName(story: "Vouchers")
        }
        else if title == "Create Brochure" {
         
              self.pushViewControllerWithStoryBoardName(story: "Brochure")
        }
        else if title == "Create Deals" {
            
          self.pushViewControllerWithStoryBoardName(story: "Deals")
            
        }
       else if title == "Free Offers" {
            self.pushViewControllerWithVCdName(story: "Deals", viewController: "FreeOfferViewController")
        }
      else  if title == "Size Chart" {
            self.pushViewControllerWithVCdName(story: "Deals", viewController: "SizeChartViewController")
        }
        else  if title == "Create Tickets" {
              self.pushViewControllerWithStoryBoardName(story: "Tickets")
        }
        else  if title == "Events" {
            self.pushViewControllerWithStoryBoardName(story: "Events")
        }
        else  if title == "Product Orders" {
            self.pushViewControllerWithStoryBoardName(story: "ProductsOrders")
        }
        else  if title == "Rent Orders" {
            self.pushViewControllerWithStoryBoardName(story: "RentOrders")
        }
        else  if title == "Trial Orders" {
            self.pushViewControllerWithStoryBoardName(story: "TrialOrders")
        }
        else  if title == "Online Booking" {
            self.pushViewControllerWithStoryBoardName(story: "Online")
        }
        else  if title == "Return Orders" {
            self.pushViewControllerWithVCdName(story: "RentOrders", viewController: "ReturnOrderViewController")
        }
        else  if title == "Cancel Orders" {
            self.pushViewControllerWithVCdName(story: "RentOrders", viewController: "CancelOrderViewController")
        }
        else  if title == "User Management" {
            self.pushViewControllerWithStoryBoardName(story: "Trial")
        }
        else  if title == "Bank Information" {
            self.pushViewControllerWithStoryBoardName(story: "Others")
        }
        
        else  if title == "Store  Information" {
            self.pushViewControllerWithVCdName(story: "Others", viewController: "StoreInfoViewController")
        }
        else  if title == "Gallery" {
            self.pushViewControllerWithVCdName(story: "Events", viewController: "GalleryViewController")
        }
        else  if title == "Review" {
            self.pushViewControllerWithVCdName(story: "Events", viewController: "ReviewsViewController")
        }
        else  if title == "Invoices" {
            self.pushViewControllerWithVCdName(story: "Others", viewController: "InvoicesViewController")
        }
        else  if title == "Instant Coupons" {
            self.pushViewControllerWithVCdName(story: "Others", viewController: "InstantCouponViewController")
        }
        else  if title == "Marketing Coupons" {
            self.pushViewControllerWithVCdName(story: "Others", viewController: "AddMarketingCouponsViewController")
        }
        
        else  if title == "Edit Profile" {
            self.pushViewControllerWithVCdName(story: "Account", viewController: "EditProfileViewController")
        }
        else  if title == "Tax Information" {
            self.pushViewControllerWithVCdName(story: "Others", viewController: "AddTaxInfoViewController")
        }
        
        else  if title == "Overview" {
            self.pushViewControllerWithVCdName(story: "Others", viewController: "PaymentOverviewViewController")
        }
        
        else  if title == "Package Material" {
            self.pushViewControllerWithVCdName(story: "Others", viewController: "PackageMaterialViewController")
        }
        
        else  if title == "Packages" {
            self.pushViewControllerWithVCdName(story: "Others", viewController: "PackagesViewController")
        }
        
    }
 

    func pushViewControllerWithStoryBoardName(story : String ){
        
        let story = UIStoryboard.init(name: story, bundle: nil)
        let viewController = story.instantiateInitialViewController()
        self.sideMenuViewController!.setContentViewController(viewController!, animated: true)
        self.sideMenuViewController!.hideMenuViewController()
    }
    
    func pushViewControllerWithVCdName(story : String ,viewController : String){
        
        
        
        let story = UIStoryboard.init(name: story, bundle: nil)
        let nav = story.instantiateViewController(withIdentifier: "Nav") as! UINavigationController
        
        let viewController = story.instantiateViewController(withIdentifier: viewController)
        
        nav.viewControllers = [viewController]
        self.sideMenuViewController!.setContentViewController(nav, animated: true)
        self.sideMenuViewController!.hideMenuViewController()
    }
    
}
