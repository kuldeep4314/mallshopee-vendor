//
//  ProductIndentifierTableViewCell.swift
//  MallshopeeVendor
//
//  Created by goyal on 14/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class ProductIndentifierTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var btnAutomatic: UIButton!
    @IBOutlet weak var btnManual: UIButton!
    @IBOutlet weak var txtValue: UITextField!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var btnSave: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if bgView != nil {
            bgView.borderView()
        }
        if txtView != nil {
            txtView.borderTextView()
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
