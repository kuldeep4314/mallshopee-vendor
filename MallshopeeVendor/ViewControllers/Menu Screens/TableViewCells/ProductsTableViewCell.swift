//
//  ProductsTableViewCell.swift
//  MallshopeeVendor
//
//  Created by goyal on 12/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class ProductsTableViewCell: UITableViewCell {

    @IBOutlet weak var btnAddSellingInfo: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnSizeAndColor: UIButton!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblStock: UILabel!
    @IBOutlet weak var lblSku: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblBrandsName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        if btnSizeAndColor != nil {
            btnSizeAndColor.layer.cornerRadius = 5.0
            btnSizeAndColor.setValue("light", forKey: "nuiClass")
            btnSizeAndColor.setValue("themeColor", forKey: "nuiClass")
        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
