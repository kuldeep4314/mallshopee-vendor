//
//  ProductDetailTableViewCell.swift
//  MallshopeeVendor
//
//  Created by goyal on 21/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class ProductDetailTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //MARK: OUTLETS

    @IBOutlet weak var sectionsContentView: UIView!
    @IBOutlet weak var sectionsView: UIView!
    @IBOutlet weak var sectionsTitleLbl: UILabel!
    
    @IBOutlet weak var valuesView: UIView!
    @IBOutlet weak var valuesTextField: UITextField!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewTextView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        if collectionView != nil {
            collectionView.delegate = self
            collectionView.dataSource = self
        }
        
//        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
//        layout.sectionInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right: 0)
//        layout.minimumInteritemSpacing = 1
//        layout.minimumLineSpacing = 0
//        collectionView!.collectionViewLayout = layout
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: FUNCTIONS
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! ProductDetailCollectionViewCell
        cell.imgCollectionView.layer.borderColor = textGrey.cgColor
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: collectionView.frame.width / 3.5, height: collectionView.frame.size.height )
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
   

}
