//
//  QCView.swift
//  MallshopeeVendor
//
//  Created by goyal on 13/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//
@objc protocol  QCFilterDelegates{
    @objc optional  func filterQCFunction(title: String)
    
}

import UIKit

class QCView: UIView {
 
    let array  = ["QC VERIFIED","QC PENDING","QC FAILED","DRAFT"]
    @IBOutlet var view: UIView!
    
    var delegaes : QCFilterDelegates?
    required init(coder aDecoder: NSCoder)  {
        super.init(coder: aDecoder)!
    }
    override init(frame: CGRect)   {
        super.init(frame: frame)
        self.setup()
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func setup() {
        view = Bundle.main.loadNibNamed("QCView", owner: self, options: nil)?[0] as? UIView
        
        view.frame = self.bounds
        view.layer.cornerRadius = 5
        view.addshodowToView()
        self.addSubview(view)
    }
    @IBAction func btnQCOptionTapped(_ sender: UIButton) {
        self.removeFromSuperview()
        delegaes?.filterQCFunction!(title: array[sender.tag - 1])
    }
    
    
}
