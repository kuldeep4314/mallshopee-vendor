//
//  MenuViewController.swift
//  MallshopeeVendor
//
//  Created by goyal on 12/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
import AKSideMenu
class MenuViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var lblCategoryBottom: UILabel!
    @IBOutlet weak var lblMenuBottom: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblMenus: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var txtStatus: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        lblMenus.textColor = appColor
        lblMenuBottom.isHidden = false
        lblCategory.textColor = UIColor.black
        lblCategoryBottom.isHidden = true
        txtSearch.leftview()
        txtStatus.leftview()
        
        self.tableView.tableFooterView = UIView()
         self.tableView.rowHeight = UITableViewAutomaticDimension
        
        // Do any additional setup after loading the view.
    }

    @IBAction func gestureMenuTapped(_ sender: UITapGestureRecognizer) {
        lblMenus.textColor = appColor
        lblMenuBottom.isHidden = false
        lblCategory.textColor = UIColor.black
        lblCategoryBottom.isHidden = true
          self.tableView.reloadData()
    }
    @IBAction func gestureCategory(_ sender: UITapGestureRecognizer) {
        lblCategory.textColor = appColor
        lblCategoryBottom.isHidden = false
        lblMenus.textColor = UIColor.black
        lblMenuBottom.isHidden = true
        self.tableView.reloadData()
    }
    
    @IBAction func btnAddMenuTapped(_ sender: UIButton) {
       
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddMenuViewController") as!
              AddMenuViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnFilterTapped(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as!
        FilterViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnMenuTapped(_ sender: UIButton) {
        
        self.sideMenuViewController!.presentLeftMenuViewController()
       
    }
    
    //MARK:- TableView Data Source Methods
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return  2
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  lblMenuBottom.isHidden == false {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MenuTableViewCell
            
            return cell
            
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellCategory", for: indexPath) as! MenuTableViewCell
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

}
