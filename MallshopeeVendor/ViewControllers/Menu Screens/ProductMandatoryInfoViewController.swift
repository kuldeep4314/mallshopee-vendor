//
//  ProductMandatoryInfoViewController.swift
//  MallshopeeVendor
//
//  Created by goyal on 14/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class ProductMandatoryInfoViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    let array = ["Color","Size","Ideal For","Other Details","Save"]
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row < array.count - 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellWithText", for: indexPath) as! ProductIndentifierTableViewCell
            cell.txtValue.placeholder = array[indexPath.row]
            return cell
            
        }
            
            
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellSave", for: indexPath) as! ProductIndentifierTableViewCell
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row < array.count - 1{
            return 45
        }
        else {
            return 200
        }
        
    }
    
}
