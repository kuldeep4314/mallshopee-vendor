//
//  EditProductViewController.swift
//  MallshopeeVendor
//
//  Created by goyal on 20/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import Foundation
import UIKit

class ProductDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK: VARIABLES

    var sectionsArray = ["Product Identifiers", "Additional Information", "Mandatory Information", "Product Image", "Product SEO"]

    var values = [["Product Name", "SKU Number", "Model No.", "Brand Name", "Category", "Last Level Category", "UPC", "EAN", "ISBN", ""], ["Decorative Material", "Fabric", "Fabric Detail", "Other Detail"], ["Color", "Size", "Ideal For", "Other Details"], [""], ["SEO Title", "Meta Keywords", "Meta Author Name", ""]]
    
    var selectedIndex = -1

    //MARK: OUTLETS
    @IBOutlet weak var viewTable: UITableView!
   
    

    override func viewDidLoad() {
        super.viewDidLoad()
        viewTable.rowHeight = UITableViewAutomaticDimension
        viewTable.tableFooterView = UIView()

    }
    
    //MARK: FUNCTIONS
    @IBAction func btnMenuTapped(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    
    
    //TABLE VIEW FUNCTIONS
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionsArray.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = viewTable.dequeueReusableCell(withIdentifier: "SectionsCell") as! ProductDetailTableViewCell
        cell.sectionsContentView.backgroundColor = UIColor.white
        cell.sectionsTitleLbl.text = sectionsArray[section]
        cell.sectionsView.layer.borderColor = textGrey.cgColor
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        
        cell.sectionsView.addGestureRecognizer(tap)
        cell.sectionsView.tag = section
        cell.sectionsView.isUserInteractionEnabled = true
        
        cell.sectionsView.addGestureRecognizer(tap)
        return cell.contentView
        //return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(selectedIndex == section) {
            if section == 0{
                return 10
            }
            else if section == 3{
                return 1
            }
            else {
                return 4
            }
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.section == 0 && indexPath.row == 9) || (indexPath.section == 4 && indexPath.row == 3) {
            let cell = viewTable.dequeueReusableCell(withIdentifier: "textViewCell", for: indexPath) as! ProductDetailTableViewCell
            cell.viewTextView.layer.borderColor = textGrey.cgColor
            return cell
        }
            
        else if indexPath.section == 3 {
            let cell = viewTable.dequeueReusableCell(withIdentifier: "CollectionTableViewCell", for: indexPath) as! ProductDetailTableViewCell
            return cell
            
        }
        
        else {
            let cell = viewTable.dequeueReusableCell(withIdentifier: "ValuesCell", for: indexPath) as! ProductDetailTableViewCell
            
            
            cell.valuesTextField.placeholder = values[indexPath.section][indexPath.row]
            cell.valuesView.layer.borderColor = textGrey.cgColor
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewTable.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK:- TAP FUNCTION
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
        if selectedIndex  == sender.view?.tag {
            selectedIndex  = -1
        }
        else{
            selectedIndex  = (sender.view?.tag)!
        }
        self.viewTable.reloadData()
    }

}


