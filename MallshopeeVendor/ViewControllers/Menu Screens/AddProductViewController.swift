//
//  AddProductViewController.swift
//  MallshopeeVendor
//
//  Created by goyal on 14/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class AddProductViewController: UIViewController ,UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDataSource,UITableViewDelegate{

    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var manualView: UIView!
    @IBOutlet weak var quickView: UIView!
    @IBOutlet weak var lblManual: UILabel!
    @IBOutlet weak var lblQuick: UILabel!
     @IBOutlet weak var tableView: UITableView!
    let arrNames = ["Product Indentifiers","Additional Information","Mandatory Information", "Product Image","Product SEO"]
    
    var indexs = 0
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
          bgView.borderView()
        self.tableView.tableFooterView = UIView()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        manualView.backgroundColor = appColor
        lblManual.textColor = UIColor.white
        quickView.backgroundColor = UIColor.white
        lblQuick.textColor = UIColor.black
        tableView.isHidden = true
         txtSearch.leftview()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func gesManualProductsTapped(_ sender: UITapGestureRecognizer) {
        manualView.backgroundColor = appColor
        lblManual.textColor = UIColor.white
        quickView.backgroundColor = UIColor.white
        lblQuick.textColor = UIColor.black
         tableView.isHidden = true
    }
    @IBAction func gestureQuickProductsTapped(_ sender: UITapGestureRecognizer) {
        quickView.backgroundColor = appColor
        lblQuick.textColor = UIColor.white
        manualView.backgroundColor = UIColor.white
        lblManual.textColor = UIColor.black
        tableView.isHidden = false
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    //MARK:- SrollView Delegates
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView == self.scrollView{
            let indexOfPage = scrollView.contentOffset.x / scrollView.frame.size.width
            
            self.collectionView.scrollToItem(at: IndexPath.init(row: Int(indexOfPage), section: 0  ), at: .centeredHorizontally, animated: true)
            indexs = Int(indexOfPage)
            self.collectionView.reloadData()
            
            
        }
    }
    
    //MARK:- CollectionView Datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrNames.count
    }
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cvCell", for: indexPath)
        
        let lbl = cell.viewWithTag(1) as! UILabel
        lbl.text = self.arrNames[indexPath.row]
        if indexs == indexPath.row {
            
            lbl.textColor = appColor
        }
        else {
            lbl.textColor = UIColor.black
            
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        
        let multiplier = CGFloat(indexPath.row)
        let cg = self.scrollView.frame.size.width * multiplier
        scrollView.scrollRectToVisible(CGRect.init(x: cg, y: 0, width: self.scrollView.frame.size.width, height: self.scrollView.frame.size.height), animated: true)
        
        
        self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        indexs = indexPath.row
        self.collectionView.reloadData()
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 170, height: 44)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    //MARK:- TableView Data Source Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return  1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:  "PRODUCTS", for: indexPath)
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
