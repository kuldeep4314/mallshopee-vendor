//
//  ProductDetailCollectionViewCell.swift
//  MallshopeeVendor
//
//  Created by goyal on 22/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class ProductDetailCollectionViewCell: UICollectionViewCell {
    
    //MARK: OUTLET
    
    @IBOutlet weak var imgCollectionView: UIImageView!
}
