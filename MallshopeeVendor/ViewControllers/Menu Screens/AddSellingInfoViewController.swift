//
//  AddSellingInfoViewController.swift
//  MallshopeeVendor
//
//  Created by user on 19/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class AddSellingInfoViewController: UIViewController {

    //MARK: VARIABLES
    var value1 = [["Seller SKU Id"],["Active"],["MRP","Your Selling Price"],["fulfilment by","Procurement type","Procurement SLA","Stock"],["Shipping Provider"],["Local Delivery Charge","Zonal delivery Charge","National Delivery Charge"],["Length","Breadth","Width","Height"],["HSN","Goods & Services tax","Luxury Cess"]]
    
    var sections = ["Listing Information","Status Detail","Price Detail","Inventory Detail","Shipping Provider","Delivery Charge to Customer","Package Detail","Tax Detail","Barcode Upload"]
    //MARK: OUTLETS
    @IBOutlet var footerView: UIView!
    @IBOutlet var btnSave: UIButton!
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        saveBtn(btn: btnSave)
        self.tableView.tableFooterView = footerView
    }
    
    //btnMenuTapped
    @IBAction func btnBackTapped(_ sender: UIButton) {
        
       self.navigationController?.popViewController(animated: true)
        
    }
    
    //ViewLAyout
    func viewLayout(view: UIView!) {
        view.layer.cornerRadius = 5
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 0.5
        view.layer.masksToBounds = false
    }
    //SaveButton
    func saveBtn(btn:UIButton) {
        
        btn.tintColor = UIColor.white
        btn.setTitle("SAVE", for: .normal)
        btn.layer.cornerRadius = 5
        btn.backgroundColor = appColor
    }
}

extension AddSellingInfoViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as? AddSellingInfoTableViewCell
        cell?.lblcell1.text = sections[section]
        if(section == 6) {
            cell?.lbl2cell1.text = "Total Number of package: 1"
        }
        else {
            cell?.lbl2cell1.text = ""
        }
        return cell?.contentView
        
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0 || section == 1 || section == 4 || section == 8) {
            return 1
        }
        else if(section == 2) {
            return 2
        }
        else if( section == 5 || section == 7) {
            return 3
        }
        else {
            return 4
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0 || indexPath.section == 1 || indexPath.section == 4 || indexPath.section == 7) {
        
        let cell =  tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as? AddSellingInfoTableViewCell
            viewLayout(view: cell?.viewCell2)
            cell?.txtfieldcell2.attributedPlaceholder = NSAttributedString(string: value1[indexPath.section][indexPath.row])
            if(indexPath.section == 1 || indexPath.section == 7) {
            cell?.imgcell2.isHidden = true
            }
            else{
                cell?.imgcell2.isHidden = false
            }
        return cell!
    }
        else if (indexPath.section == 2 || indexPath.section == 5) {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath) as? AddSellingInfoTableViewCell
            viewLayout(view: cell?.viewCell3)
            cell?.txtfieldcell3.attributedPlaceholder = NSAttributedString(string: value1[indexPath.section][indexPath.row])
            cell?.lbl3cell3.text = "INR"
            return cell!
        }
        else if (indexPath.section == 3) {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as? AddSellingInfoTableViewCell
            viewLayout(view: cell?.viewCell2)
            cell?.txtfieldcell2.attributedPlaceholder = NSAttributedString(string: value1[indexPath.section][indexPath.row])
            if(indexPath.row == 2) {
                cell?.imgcell2.isHidden = true
            }
            else {
            cell?.imgcell2.isHidden = false
            }
            return cell!
        }
        else if (indexPath.section == 6) {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath) as? AddSellingInfoTableViewCell
            viewLayout(view: cell?.viewCell3)
            cell?.txtfieldcell3.attributedPlaceholder = NSAttributedString(string: value1[indexPath.section][indexPath.row])
            cell?.lbl3cell3.text = "cms"
            if(indexPath.row == 3) {
                cell?.lbl3cell3.text = "kgs"
            }
            return cell!
        }
        else{
        
        let cell =  tableView.dequeueReusableCell(withIdentifier: "cell4", for: indexPath) as? AddSellingInfoTableViewCell
          viewLayout(view:  cell?.btncell4)
        return cell!
        }
    }

}
