//
//  AddMenuViewController.swift
//  MallshopeeVendor
//
//  Created by goyal on 12/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class AddMenuViewController: UIViewController ,UITextViewDelegate {

    let textViewPlaceHolder = "Enter Your Description"
    @IBOutlet weak var txtView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        for i in 1..<6 {
            let vc = self.view.viewWithTag(i)
            vc?.borderView()
        }
        self.txtView.text = textViewPlaceHolder
        self.txtView.textColor = textGrey
        txtView.borderTextView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- UITEXTVIEW DELEGATES
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        
        if self.txtView.textColor == textGrey
        {
            self.txtView.text = ""
            self.txtView.textColor = UIColor.black
            print("begin working")
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        
        if self.txtView.text.isEmpty {
            self.txtView.text =  textViewPlaceHolder
            self.txtView.textColor = textGrey
            print("end working")
        }
    }


}
