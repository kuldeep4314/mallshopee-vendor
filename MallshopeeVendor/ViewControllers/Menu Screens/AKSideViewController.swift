//
//  AKSideViewController.swift
//  MallshopeeVendor
//
//  Created by goyal on 12/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
import AKSideMenu
class AKSideViewController: AKSideMenu {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.contentViewController =  self.storyboard?.instantiateViewController(withIdentifier: "Nav")
        self.leftMenuViewController = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuViewController")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   

}
