//
//  RentOrderView.swift
//  MallshopeeVendor
//
//  Created by user on 19/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class RentOrderView: UIView {

    //MARK: VARIABLES
    var delegate: RentOrder! = nil
    
    //MARK: OUTLETS
    @IBOutlet var view: UIView!
    @IBOutlet var outerView: UIView!
    @IBOutlet var viewTxt: UIView!
    @IBOutlet var img: UIImageView!
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var lblMsg: UILabel!
    @IBOutlet var textView: UITextView!
    
    //MARK: ACTIONS
    @IBAction func btnSubmit(_ sender: UIButton) {

        delegate.myRentOrderDidFinish(controller: self, description: textView.text!)
        self.removeFromSuperview()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    

    //setUp
    func setUp() {
        self.view = Bundle.main.loadNibNamed("RentOrderView", owner: self, options: nil)?.first as? UIView!
        self.view.frame = self.bounds
        self.view.backgroundColor = UIColor(white: 0, alpha: 0.5)
        self.addSubview(self.view!)
        textView.delegate = self
        btnLayout(btn: btnSubmit)
        imgLayout(img: img)
        viewLayout(view: viewTxt)
        viewLayout(view: outerView)
        lblMsg.text = "Please give the reason of cancel this order"
        
    }
    //viewLayout
    func viewLayout(view: UIView) {
        view.layer.cornerRadius = 5
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 0.5
        view.layer.masksToBounds = false
    }
    
    //btnLayout
    func btnLayout(btn: UIButton){
        btn.layer.cornerRadius = 5
    }
    
    //imgViewLayout
    func imgLayout(img: UIImageView) {
        img.layer.cornerRadius = self.img.frame.width / 2
        
    }
}
extension RentOrderView: UITextViewDelegate {
    //textViewDelegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView.textColor == UIColor.lightGray) {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if(textView.text.isEmpty) {
            textView.text = "Enter your reason here..."
            textView.textColor = UIColor.lightGray
        }
    }
}
//protocol
protocol RentOrder {
    func myRentOrderDidFinish(controller: RentOrderView,description: String)
}

