//
//  SizeChartViewController.swift
//  MallshopeeVendor
//
//  Created by goyal on 15/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class SizeChartViewController:UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    @IBOutlet weak var tableView: UITableView!
    let array = ["Select Product","Standard Size Chart","Custom Size Chart","Size Chart Image","Save"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    @IBAction func btnMenuTapped(_ sender: Any) {
          self.sideMenuViewController!.presentLeftMenuViewController()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellWithText", for: indexPath) as! SizeChartTableViewCell
            cell.txtValue.text = array[indexPath.row]
            return cell
            
        }
            
        else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellChart", for: indexPath) as! SizeChartTableViewCell
           cell.lblChartType.text  = array[indexPath.row]
            return cell
            
        }
            
        else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellChart", for: indexPath) as! SizeChartTableViewCell
               cell.lblChartType.text  = array[indexPath.row]
            return cell
            
        }
            
        else if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellImage", for: indexPath) as! SizeChartTableViewCell
            
            return cell
        }
            
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellSave", for: indexPath) as! SizeChartTableViewCell
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
           return 45
        }
        else{
           return 130
        }
        
    }
    
   
    
}
