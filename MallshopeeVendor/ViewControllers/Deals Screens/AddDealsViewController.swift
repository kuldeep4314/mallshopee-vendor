//
//  AddDealsViewController.swift
//  MallshopeeVendor
//
//  Created by goyal on 14/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class AddDealsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource ,UITextViewDelegate{
     let textViewPlaceHolder = "Deal Description"
    @IBOutlet weak var tableView: UITableView!
    let array = ["Deal Name","Deal Price","Deal Description","Deal Start date","Deal Start Time","Deal Image","Save"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row < 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellWithText", for: indexPath) as! DealsTableViewCell
          cell.txtValue.text = array[indexPath.row]
            return cell
            
        }
            
        else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellTxtView", for: indexPath) as! DealsTableViewCell
            cell.txtView.text = textViewPlaceHolder
            cell.txtView.textColor = textGrey
            cell.txtView.delegate = self
            return cell
            
        }
            
        else if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellDate", for: indexPath) as! DealsTableViewCell
            cell.txtStop.placeholder = array [indexPath.row]
            cell.txtStart.placeholder = array [indexPath.row]
            return cell
            
        }
            
        else if indexPath.row == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellDate", for: indexPath) as! DealsTableViewCell
             cell.txtStop.placeholder = array [indexPath.row]
              cell.txtStart.placeholder = array [indexPath.row]
            return cell
            
        }
        else if indexPath.row == 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellImage", for: indexPath) as! DealsTableViewCell
          
            return cell
        }
        
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellSave", for: indexPath) as! DealsTableViewCell
          
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 2 || indexPath.row == 5 || indexPath.row == 6{
              return 120
        }
        
        return 45
    }
    
    //MARK:- UITEXTVIEW DELEGATES
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.textColor == textGrey
        {
            textView.text = ""
            textView.textColor = UIColor.black
            print("begin working")
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        
        if textView.text.isEmpty {
            textView.text =  textViewPlaceHolder
            textView.textColor = textGrey
            print("end working")
        }
    }

}

