//
//  DealsTableViewCell.swift
//  MallshopeeVendor
//
//  Created by goyal on 14/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class DealsTableViewCell: UITableViewCell {

    @IBOutlet weak var dealName: UILabel!
    @IBOutlet weak var dealStopTime: UILabel!
    @IBOutlet weak var dealStopDate: UILabel!
    @IBOutlet weak var dealImage: UIImageView!
    @IBOutlet weak var dealStartTime: UILabel!
    @IBOutlet weak var dealStartDate: UILabel!
    @IBOutlet weak var dealDescription: UILabel!
    @IBOutlet weak var dealPrice: UILabel!
    @IBOutlet weak var txtStop: UITextField!
    @IBOutlet weak var txtStart: UITextField!
    @IBOutlet weak var imgStop: UIImageView!
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var txtValue: UITextField!
    @IBOutlet weak var imgStart: UIImageView!
    @IBOutlet weak var startView: UIView!
    @IBOutlet weak var stopView: UIView!
    @IBOutlet weak var btnAddImage: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        if bgView != nil {
            
            bgView.borderView()
        }
        
        if txtView != nil {
            txtView.borderTextView()
        }
        
        if startView != nil {
            startView.borderView()
            stopView.borderView()
        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
