//
//  SizeChartTableViewCell.swift
//  MallshopeeVendor
//
//  Created by goyal on 15/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class SizeChartTableViewCell: UITableViewCell {

    @IBOutlet weak var lblChartType: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var txtValue: UITextField!
    @IBOutlet weak var bgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        if bgView != nil {
            bgView.borderView()
        }
        if mainView != nil {
            mainView.borderView()
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
