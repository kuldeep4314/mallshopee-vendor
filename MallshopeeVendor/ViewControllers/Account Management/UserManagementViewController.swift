//
//  UserManagementViewController.swift
//  MallshopeeVendor
//
//  Created by Avatar Singh on 2018-03-18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class UserManagementViewController :UIViewController ,UITableViewDataSource,UITableViewDelegate{
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var txtStatus: UITextField!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        txtSearch.leftview()
        txtStatus.leftview()
        
        self.tableView.tableFooterView = UIView()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnFilterTapped(_ sender: Any) {
        let story = UIStoryboard.init(name: "Menu", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func btnMenuTapped(_ sender: Any) {
        self.sideMenuViewController!.presentLeftMenuViewController()
        
    }
    @IBAction func btnAddTapped(_ sender: Any) {
        
        
    }
    //MARK:- TableView Data Source Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return  2
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! UserManagementTableViewCell
        
        return cell
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
      
        
        
    }
    
    
}

