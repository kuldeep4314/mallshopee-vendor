//
//  EditProfileViewController.swift
//  MallshopeeVendor
//
//  Created by user on 14/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController {

    //MARK: OUTLETS
    @IBOutlet var personalView: UIView!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var nameView: UIView!
    @IBOutlet var idView: UIView!
    @IBOutlet var phnNoView: UIView!
    @IBOutlet var ownedView: UIView!
    @IBOutlet var textView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    //MARK: setup
    func setup() {
       viewLayout(view: personalView)
        viewLayout(view: nameView)
        viewLayout(view: idView)
        viewLayout(view: phnNoView)
        viewLayout(view: ownedView)
        viewLayout(view: textView)
        imgLayout(img: imgProfile)
    }

    //btnMenuTapped
    @IBAction func btnMenuTapped(_ sender: UIButton) {
        
        self.sideMenuViewController!.presentLeftMenuViewController()
        
    }
    
    //MARK: viewLayout
    func viewLayout(view: UIView!) {
        view.layer.cornerRadius = 5
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 0.5
        view.layer.masksToBounds = false
    }
    
    //MARK: imgLayout
    func imgLayout(img: UIImageView) {
        img.layer.cornerRadius = self.imgProfile.frame.width / 2
        
    }
}
