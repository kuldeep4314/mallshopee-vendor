//
//  VoucherIndentifierTableViewCell.swift
//  MallshopeeVendor
//
//  Created by goyal on 13/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class VoucherIndentifierTableViewCell: UITableViewCell {

    @IBOutlet weak var stopBgView: UIView!
    @IBOutlet weak var startBgView: UIView!
    @IBOutlet weak var txtStop: UITextField!
    @IBOutlet weak var txtStart: UITextField!
    @IBOutlet weak var imgStop: UIImageView!
    @IBOutlet weak var imgStart: UIImageView!
    @IBOutlet weak var heightCons: NSLayoutConstraint!
    @IBOutlet weak var lblImageRatio: UILabel!
    @IBOutlet weak var btnImage: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtValue: UITextField!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var txtBgView: UIView!
    @IBOutlet weak var activeBgView: UIView!
    @IBOutlet weak var activeImg: UIImageView!
    
    @IBOutlet weak var txtStopTime: UITextField!
    @IBOutlet weak var txtStartTime: UITextField!
    @IBOutlet weak var lblDayName: UILabel!
    @IBOutlet weak var lblActive: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        if txtBgView != nil {
            txtBgView.borderView()
        }
        if startBgView != nil {
            startBgView.borderView()
            stopBgView.borderView()
        }
        if txtStartTime != nil {
            txtStartTime.borderTextField()
            txtStartTime.leftview()
            txtStopTime.leftview()
            txtStopTime.borderTextField()
        }
       
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
