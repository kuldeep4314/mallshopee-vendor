//
//  VoucherIndentifierViewController.swift
//  MallshopeeVendor
//
//  Created by goyal on 13/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class VoucherIndentifierViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    let array = ["Voucher Name","MRP Price","Voucher Offer Price","Brand Name","Category","Procurement Type","Stock Count","Minimum Stock","Product SKU","Seller SKU","Barcode Image","Start date","Start Time","Product/Service Image","Deal TnCs","Voucher Active Days","Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday",""]
    
    override func viewDidLoad() {
        super.viewDidLoad()
      tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row < 10 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellWithText", for: indexPath) as! VoucherIndentifierTableViewCell
             cell.txtValue.placeholder = array[indexPath.row]
            return cell
            
        }
        
       else if indexPath.row == 10 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellImage", for: indexPath) as! VoucherIndentifierTableViewCell
           cell.heightCons.constant = 0
            cell.lblTitle.text = array[indexPath.row]
            return cell
            
        }
        
        else if indexPath.row == 11 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellDate", for: indexPath) as! VoucherIndentifierTableViewCell
             cell.txtStart.placeholder = array[indexPath.row]
              cell.txtStop.placeholder = array[indexPath.row]
            return cell
            
        }
        
        else if indexPath.row == 12 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellDate", for: indexPath) as! VoucherIndentifierTableViewCell
            cell.txtStart.placeholder = array[indexPath.row]
            cell.txtStop.placeholder = array[indexPath.row]
            return cell
            
        }
        else if indexPath.row == 13 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellImage", for: indexPath) as! VoucherIndentifierTableViewCell
            cell.heightCons.constant = 15
            cell.lblTitle.text = array[indexPath.row]
            return cell
        }
        
        else if indexPath.row == 14 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellWithText", for: indexPath) as! VoucherIndentifierTableViewCell
            cell.txtValue.placeholder = array[indexPath.row]
            return cell
            
            
        }
        else if indexPath.row == 15 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellActive", for: indexPath) as! VoucherIndentifierTableViewCell
            cell.lblActive.text = array[indexPath.row]
            return cell
            
            
        }
        
        else if indexPath.row == array.count - 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellSave", for: indexPath) as! VoucherIndentifierTableViewCell
            
            return cell
            
        }
        else {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellDays", for: indexPath) as! VoucherIndentifierTableViewCell
         cell.lblDayName.text = array[indexPath.row]
        return cell
        }
    }
  
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         if indexPath.row < 10 {
             return 45
        }
       else if indexPath.row == 10 || indexPath.row == 13{
            return 150
        }
         else if indexPath.row == array.count - 1{
            
            return 100
        }
        
        return 45
    }
    
    
}
