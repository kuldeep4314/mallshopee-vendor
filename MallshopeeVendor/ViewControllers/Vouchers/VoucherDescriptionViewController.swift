//
//  VoucherDescriptionViewController.swift
//  MallshopeeVendor
//
//  Created by goyal on 13/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class VoucherDescriptionViewController: UIViewController {

    @IBOutlet weak var txtViewDescription: UITextView!
     @IBOutlet weak var txtViewRedeem: UITextView!
      @IBOutlet weak var txtViewCancelPolicy: UITextView!
   
    let placeHolders = ["Voucher Description","How to redeem","Cancelletion Policy"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for i in 1..<4 {
            let txtView = self.view.viewWithTag(i) as! UITextView
             txtView.borderTextView()
            txtView.textColor = textGrey
            txtView.text = placeHolders[i - 1]
        }
  
        
     
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- UITEXTVIEW DELEGATES
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        
        if textView.textColor == textGrey
        {
            textView.text = ""
           textView.textColor = UIColor.black
            print("begin working")
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        
        if textView.text.isEmpty {
            if textView.tag == 1 {
                textView.text =  placeHolders[0]
            }
            else if textView.tag  == 2 {
                 textView.text =   placeHolders[1]
            }
            else{
               textView.text =   placeHolders[2]
            }
            
          textView.textColor = textGrey
            print("end working")
        }
    }

  

}
