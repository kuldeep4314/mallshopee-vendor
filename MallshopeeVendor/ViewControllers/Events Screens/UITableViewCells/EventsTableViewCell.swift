//
//  EventsTableViewCell.swift
//  MallshopeeVendor
//
//  Created by goyal on 15/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class EventsTableViewCell: UITableViewCell {

    @IBOutlet weak var txtStop: UITextField!
    @IBOutlet weak var txtStart: UITextField!
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var txtValue: UITextField!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var startView: UIView!
    @IBOutlet weak var stopView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        if bgView != nil {
            
            bgView.borderView()
        }
        
        if txtView != nil {
            txtView.borderTextView()
        }
        
        if startView != nil {
            startView.borderView()
            stopView.borderView()
        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
