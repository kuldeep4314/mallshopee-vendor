//
//  TableViewCell.swift
//  MallshopeeVendor
//
//  Created by goyal on 13/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class ReviewsTableViewCell: UITableViewCell {
    
    //MARK: OUTLETS

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgEmoji: UIImageView!
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnReply: UIButton!
    @IBOutlet weak var btnMessage: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var txtFieldReply: UITextField!
    @IBOutlet weak var replyHeightConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
