//
//  GalleryTableViewCell.swift
//  MallshopeeVendor
//
//  Created by goyal on 13/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class GalleryTableViewCell: UITableViewCell {
    
    //MARK: OUTLETS

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgDelete: UIImageView!
    @IBOutlet weak var imgGallery: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnActive: UIButton!
    @IBOutlet weak var lblActive: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
