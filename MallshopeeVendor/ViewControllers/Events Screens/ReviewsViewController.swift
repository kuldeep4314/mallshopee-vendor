//
//  Reviews.swift
//  MallshopeeVendor
//
//  Created by goyal on 13/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import Foundation
import UIKit

class ReviewsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: OUTLETS
    var replyIndex = -1
    @IBOutlet weak var viewTable: UITableView!
    
    //MARK: ACTIONS
    @objc func replyAction(_ sender: UIButton) {
      
        if sender.tag == replyIndex {
           replyIndex = -1
        }
        else{
            replyIndex = sender.tag
        }
    
        viewTable.reloadData()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewTable.rowHeight = UITableViewAutomaticDimension
    }
    @IBAction func btnMenuTapped(_ sender: Any) {
        self.sideMenuViewController?.presentLeftMenuViewController()
    }
    
    
    //MARK: FUNCTIONS
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewTable.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = viewTable.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ReviewsTableViewCell
        

        cell.txtFieldReply.leftview()

        
        cell.btnReply.tag = indexPath.row

        cell.btnReply.addTarget(self, action: #selector(self.replyAction(_:)), for: .touchUpInside)

        if replyIndex == indexPath.row {
            cell.replyHeightConstraint.constant = 40
        }
        else{
            cell.replyHeightConstraint.constant = 0
        }
        
        self.setBorder(textField: cell.txtFieldReply)
        return cell
    }
    
    
    func setBorder(textField : UITextField){
        
      textField.layer.borderWidth = 1
      textField.layer.borderColor = textGrey.cgColor
      textField.layer.cornerRadius = 8

        
    }
    
}
