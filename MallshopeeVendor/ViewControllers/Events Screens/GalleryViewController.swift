//
//  Gallery.swift
//  MallshopeeVendor
//
//  Created by goyal on 13/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import Foundation
import UIKit

class GalleryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: IBOUTLETS
    
    @IBOutlet weak var viewTable: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    @IBAction func btnMenuTapped(_ sender: UIButton) {
        
        self.sideMenuViewController!.presentLeftMenuViewController()
        
    }
    
    //MARK: FUNCTIONS
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = viewTable.dequeueReusableCell(withIdentifier: "Cell2", for: indexPath) as! GalleryTableViewCell
        cell.lblName.text = "John Doe"
        
        cell.lblDate.text = "Jan 12, 2018"
        cell.btnActive.layer.borderWidth = 1
        
        return cell
    }
    
}
