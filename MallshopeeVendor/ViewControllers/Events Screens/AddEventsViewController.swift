//
//  AddEventsViewController.swift
//  MallshopeeVendor
//
//  Created by goyal on 15/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//


import UIKit

class AddEventsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource ,UITextViewDelegate{
    let textViewPlaceHolder = "Enter Meta Description"
    @IBOutlet weak var tableView: UITableView!
    let array = ["Event Name","Event Price","Voucher Start date","Voucher Start Time","Enter Meta Description","Save"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row < 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellWithText", for: indexPath) as! EventsTableViewCell
            cell.txtValue.text = array[indexPath.row]
            return cell
            
        }
            
        else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellDate", for: indexPath) as! EventsTableViewCell
            cell.txtStop.placeholder = array [indexPath.row]
            cell.txtStart.placeholder = array [indexPath.row]
            return cell
            
        }
            
        else if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellDate", for: indexPath) as! EventsTableViewCell
            cell.txtStop.placeholder = array [indexPath.row]
            cell.txtStart.placeholder = array [indexPath.row]
            return cell
            
        }
            
        else if indexPath.row == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellTxtView", for: indexPath) as! EventsTableViewCell
            cell.txtView.text = textViewPlaceHolder
            cell.txtView.textColor = textGrey
            cell.txtView.delegate = self
            return cell
            
        }
        
            
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellSave", for: indexPath) as! EventsTableViewCell
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == array.count - 1 {
            return 200
        }
        else if indexPath.row == array.count - 2 {
            return 100
        }
     
        return 50
    }
    
    //MARK:- UITEXTVIEW DELEGATES
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.textColor == textGrey
        {
            textView.text = ""
            textView.textColor = UIColor.black
            print("begin working")
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        
        if textView.text.isEmpty {
            textView.text =  textViewPlaceHolder
            textView.textColor = textGrey
            print("end working")
        }
    }
    
}


