//
//  EventsViewController.swift
//  MallshopeeVendor
//
//  Created by goyal on 15/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
import AKSideMenu
class EventsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var txtStatus: UITextField!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        txtSearch.leftview()
        txtStatus.leftview()
        self.tableView.tableFooterView = UIView()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnMenuTapped(_ sender: UIButton) {
        
        self.sideMenuViewController!.presentLeftMenuViewController()
        
    }
    @IBAction func btnFilterTapped(_ sender: Any) {
        let story = UIStoryboard.init(name: "Menu", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func btnAddVouchersTapped(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddEventsViewController") as! AddEventsViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    //MARK:- TableView Data Source Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return  1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}



