

@AppColor : #FC5655;

@lightRed : #00cdd2;

@lightGray : #E9E9E9;

@textGrey : #B2B2B2;

themeColor {
background-color:@AppColor;
}



lightGray {
background-color:@lightGray;
}

textGrey {
font-color :@textGrey;
}
gray{
font-color :@lightGray;
}

red {
font-color :@AppColor;
}

lightRed {
font-color :@lightRed;
}

@black: Montserrat-Black;
@bold: Montserrat-Bold;
@extraBold: Montserrat-ExtraBold;
@extraLight: Montserrat-ExtraLight;
@light: Montserrat-Light;
@medium: Montserrat-Medium;
@regular: Montserrat-Regular;
@semiBold: Montserrat-SemiBold;
@thin: Montserrat-Thin;

black {
font-name: @black;
}

bold {
font-name: @bold;
}

extraBold {
font-name: @extraBold;
}

extraLight {
font-name: @extraLight;
}

light {
font-name: @light;
}

medium {
font-name: @medium;
}

regular {
font-name: @regular;
}

semiBold {
font-name: @semiBold;
}

thin {
font-name: @thin;
}


